###
# Autoscaling Group utilities.
#
DMA_ASG_UTILS_VERSION=1

# AWS Commands require a region. The AWS_DEFAULT_REGION
# is set in /opt/dma/bin/env.sh.
. /opt/dma/bin/env.sh

if [ -z "$DMA_EC2_UTILS_VERSION" ] ; then
    . /opt/dma/bin/ec2Utils.sh
fi

###
# Get the ASG name for an instance. Use the current instance
# if no instance-id is provided.
# param: instanceId the instance-id of the instance to query for its ASG name.
#        Optional, if not specified, uses the current instance.
# return: string name of the ASG for the instance, or None if the instance
#        is not registered with an ASG.
#
function asgNameForEc2Instance() {
    local instanceId="$1"
    if [ -z "$instanceId" ] ; then
        instanceId=$(ec2InstanceId)
    fi
    if [ -n "$instanceId" ] ; then
        aws autoscaling describe-auto-scaling-instances \
            --instance-ids $instanceId \
            --query "AutoScalingInstances[0].AutoScalingGroupName" \
            --output text
    fi
}

###
# Find the ASG by name. The match will be on any part of
# the ASG name. This will return all ASG names that
# match the input parameter where the input is exactly the same
# as the ASG name, or the input is a substring of the ASG name.
#
# Assuming the search string is main-dmamasterasg then:
# Name                                    Matches
# ----------------------------            -------
# main-dmamasterasg-ABCDEF123             true
# main-dmamasterasg-123456789             true
# dmamasterasg-main-123456789             false
# bvmain-dmamasterasg-ABCDEF123           true
# sec-dmamasterasg-ABCDEF123              false
# main-dmamaster-ABCDEF123                false
#
# param: name the name to match
# return: names of matching ASGs, one per line
function asgFindByNameContains() {
    aws autoscaling describe-auto-scaling-groups \
        --query "AutoScalingGroups[*].[AutoScalingGroupName]" \
        --output text | \
    while read asgName ; do
        if [[ "$asgName" =~ $1 ]]
        then
            echo "$asgName"
        fi
    done
}

###
# The load balancers attached to one or more ASGs.
# param asgName the names of the ASG to query,
#               or the current ASG if unspecified
# return the list of ELB names, one per line
function asgLoadBalancers() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query "AutoScalingGroups[0].LoadBalancerNames" \
        --output text
}

###
# Describe the ASG named, or the current ASG if no name is specified.
# param: asgName the name of the ASG to query
#                If no name, use the current ASG name
# return: the ASG description from the aws autoscaling
#         describe-auto-scaling-groups command.
function asgDescribe() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    if [ -n "$asgName" ] ; then
        aws autoscaling describe-auto-scaling-groups \
            --auto-scaling-group-names "$asgName" \
            --output text
    fi
}

###
# List the instance-ids of the ASG provided.
# param: asgName the name of the ASG to query
#                If no name, use the current ASG name: $(asgName).
# return: instance-ids, one per line
function asgEc2InstanceIds() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query "AutoScalingGroups[0].Instances[*].[InstanceId]" \
        --output text
}

###
# Given an ASG name, return the desired number of instances.
# param asgName the name of the ASG to query
#               If no name, use the current ASG name: $(asgName).
# return int the desired instance count for the specified ASG
function asgDesiredInstanceCount() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query "AutoScalingGroups[0].DesiredCapacity" \
        --output text
}

###
# Given an ASG name, return the current number of instances.
# param asgName the name of the ASG to query
#               If no name, use the current ASG name: $(asgName).
# return int the current instance count for the specified ASG
function asgCurrentInstanceCount() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    asgEc2InstanceIds "$asgName" | wc -l
}

###
# Given an ASG name, return the number of instances in the
# ASG that are healthy and in service. Do not count an instance as
# healthy if it is not in service.
function asgHealthyInstanceCount() {
    local asgName=${1:-$(asgNameForEc2Instance)}
    aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query "AutoScalingGroups[0].Instances[*].[HealthStatus,LifecycleState]" \
        --output text | \
        grep '^Healthy' | grep 'InService' | wc -l
}

###
# Count the instances that are not both health and in service.
# An instance needs to be both healthy and in service to be considered
# truly healthy.
function asgUnhealthyInstanceCount() {
    local asgName="${1:-$(asgNameForEc2Instance)}"
    local count=0
    aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query "AutoScalingGroups[0].Instances[*].[HealthStatus,LifecycleState]" \
        --output text | \
    while read health inService ; do
        if [ "$health" != "Healthy" ] || [ "$inService" != "InService" ] ; then
            count=$((count+1))
        fi
    done

    echo $count
}

###
# Given the instanceId, determine its state in its ASG.
# param instanceId the instanceId to check, or the current instance if
#                  not specified.
# return the instance state in its ASG
function asgInstanceState() {
    local instanceId="${1:-$(ec2InstanceId)}"
    local asgName=$(asgNameForEc2Instance $instanceId)
    if [ -z "$asgName" ] ; then
        echo "NoAsg"
    else
        aws autoscaling describe-auto-scaling-groups \
            --auto-scaling-group-names $asgName \
            --query "AutoScalingGroups[0].Instances[?InstanceId=='$instanceId'].LifecycleState" \
            --output=text
    fi
}

###
# The private IP addresses of all the EC2 instances in an ASG.
# param 1: the ASG name, or if not specified the ASG of the
#          current instance is used.
# return: The private IP address of each instance in the ASG.
function asgInstancesPrivateIps() {
    local myAsgName="${1:-$(asgNameForEc2Instance)}"
    ec2PrivateIpAddress $(asgEc2InstanceIds "$myAsgName")
}