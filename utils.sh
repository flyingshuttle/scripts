###
# EC2 Utilities
#
DMA_EC2_UTILS_VERSION=1

###
# Get the instance metadata from AWS
# param1: the metadata key to retrieve
function ec2InstanceMetadata() {
    curl -m ${AWS_EC2_METADATA_TIMEOUT:-10} -s http://169.254.169.254/latest/meta-data/$1
}

###
# Get the instanceId of the current instance.
# return the current instance instanceId
function ec2InstanceId() {
    ec2InstanceMetadata instance-id
}

###
# Get the region for the specified instance(s). Return the region
# of the current instance if no instance provided.
# param1: instance-id ...
# return: the regions for the specified instances, one per line, or
#  the current instance if no instance-id is provided.
function ec2InstanceRegion() {
    if [ $# -eq 0 ] ; then
        $(ec2InstanceMetadata "placement/availability-zone") | sed -e 's/[a-z]$//'
    else
        aws ec2 describe-instances \
            --instance-ids $@ \
            --output text \
            --query 'Reservations[*].Instances[*].[PrivateIpAddress].[Placement.AvailabilityZone]' \
        | sed -e 's/[a-z]$//'
    fi
}

###
# The private IP addresses of the specified instance-ids.
# param: instance-id ...
# return: the instance private IP addresses, one per line
function ec2PrivateIpAddress() {
    if [ $# -eq 0 ] ; then
        ec2InstanceMetadata 'local-ipv4'
    else
        aws ec2 describe-instances \
            --instance-ids $@ \
            --output text \
            --query 'Reservations[*].Instances[*].[PrivateIpAddress]'
    fi
}

###
# Get the vpc-id of the instance.
# param: instance-id the instance-id to retrieve, or if
#                    not provided, uses the current instance
# return: the vpc-id of the instance
function ec2VpcId() {
    local instanceId="$1"
    if [ -z "$instanceId" ] ; then
        instanceId=$(ec2InstanceId)
    fi
    aws ec2 describe-instances \
        --instance-ids $@ \
        --output text \
        --query 'Reservations[*].Instances[*].[VpcId]'
}