#!/bin/bash
set 

DIRECTOR_DIR=/apps/fmmis/MessageAutomation
START_DIR=`pwd`
RELEASE_DIR=${0%/*}

if [ -n "$1" ];then
  DIRECTOR_DIR=$1
fi

cd $RELEASE_DIR

echo "Installing Gateway to $DIRECTOR_DIR"

mkdir -p $DIRECTOR_DIR

$DIRECTOR_DIR/director-server-4.5.0/bin/director stop

# Remove all except for director.properties and data directory
find  $DIRECTOR_DIR/director-server-4.5.0/deploy/scb_ordm ! -path "*/data/*" -type f ! -name 'director.properties' -delete

cp -r scb_ordm_delivery_release_*/scb_ordm $DIRECTOR_DIR/director-server-4.5.0/deploy

cd $START_DIR

echo "Install finished successfully"