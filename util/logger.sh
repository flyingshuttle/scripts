#!/bin/bash

SCRIPT_LOG_PATH=$1
touch $SCRIPT_LOG_PATH

function SCRIPTENTRY(){
 timeAndDate=`date`
 script_name=`basename "$0"`
 script_name="${script_name%.*}"
 echo "[$timeAndDate] [DEBUG]  > $script_name $FUNCNAME" >> $SCRIPT_LOG_PATH
}

function SCRIPTEXIT(){
 script_name=`basename "$0"`
 script_name="${script_name%.*}"
 echo "[$timeAndDate] [DEBUG]  < $script_name $FUNCNAME" >> $SCRIPT_LOG_PATH
}

function ENTRY(){
 local cfn="${FUNCNAME[1]}"
 timeAndDate=`date`
 echo "[$timeAndDate] [DEBUG]  > $cfn $FUNCNAME" >> $SCRIPT_LOG_PATH
}

function EXIT(){
 local cfn="${FUNCNAME[1]}"
 timeAndDate=`date`
 echo "[$timeAndDate] [DEBUG]  < $cfn $FUNCNAME" >> $SCRIPT_LOG_PATH
}

function INFO(){
 local function_name="${FUNCNAME[1]}"
 local msg="$1"
 timeAndDate=`date`
 echo "[$timeAndDate] [INFO]  $msg" >> $SCRIPT_LOG_PATH
}

function DEBUG(){
 local function_name="${FUNCNAME[1]}"
 local msg="$1"
 timeAndDate=`date`
 echo "[$timeAndDate] [DEBUG]  $msg" >> $SCRIPT_LOG_PATH
}

function SUCCESS(){
 local function_name="${FUNCNAME[1]}"
 local msg="$1"
 timeAndDate=`date`
 echo "[$timeAndDate] [SUCCESS]  $msg" >> $SCRIPT_LOG_PATH
}

function ERROR(){
 local function_name="${FUNCNAME[1]}"
 local msg="$1"
 timeAndDate=`date`
 echo "[$timeAndDate] [ERROR]  $msg" >> $SCRIPT_LOG_PATH
}