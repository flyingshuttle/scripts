#!/bin/bash
# Print the sum of RSS (Resident Set Size, the non-swapped physical memory that a process is using)
# for all child processes of Docker daemon (dockerd) and rest of processes running.
#
# Use --raw parameter to get only values delimited with TAB for better processing

# Find the Docker daemon PID with pgrep
# Note: the PID is also stored in /var/run/docker.pid
dockerdpid=$(pgrep -l dockerd | awk '{print $1}')

# Exit if Docker daemon PID cannot be found
if [ -z $dockerdpid ]; then
    echo "Docker daemon is not running."
    exit 1
fi

# Get all PIDs 'under' Docker daemon recursively (the process launched inside Docker container
# by another process running inside has no Docker daemon PID as its parrent, thus we need to traverse the process tree)
# Credits: https://superuser.com/questions/363169/ps-how-can-i-recursively-get-all-child-process-for-a-given-pid
childpids() {
    echo -n $1
    for cpid in $(ps -o pid --no-headers --ppid $1); do
        echo -n ",$cpid,$(childpids $cpid)"
    done
}

# All PIDs started under Docker daemon (delimited with comma)
dockerdchildpids=$(childpids $dockerdpid)

# RSS from ps is reported in KiB, divide with 1024 to get MiB
units=1024

# Total ECS memory (ps for only Docker daemon PIDs) in MiB
totalECS=$(ps -o pid,ppid,rss,cmd -p $dockerdchildpids | awk -v u=$units '{rss += $3} END {printf "%.0f", rss / u}')

# Total Other memory (ps for all EXCEPT Docker daemon PIDs) in MiB
totalOther=$(ps -o pid,ppid,rss,cmd -p $dockerdchildpids -N | awk -v u=$units '{rss += $3} END {printf "%.0f", rss / u}')

# Print the results
if [ "$1" == "--raw" ]; then
    echo -e "$totalECS\t$totalOther"
else
    echo -e "Total ECS:\t$totalECS MiB"
    echo -e "Total Other:\t$totalOther MiB"
fi